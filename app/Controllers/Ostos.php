<?php namespace App\Controllers;

use App\Models\OstosModel;

class Ostos extends BaseController
{
	public function index()
	{
    $ostos_model = new OstosModel();
    $data['ostokset'] = $ostos_model->haeOstokset();
    $data['maara'] = count($data['ostokset']);
		return view('ostos_view',$data);
  }
  
  public function lisaa() {
    $ostos_model = new OstosModel();
    $ostos_model->save([
      'kuvaus' => $this->request->getVar('kuvaus')
    ]);
    return redirect('ostos');
  }

  // poistetaan tieto CI:n sisäänrakennetulla CRUD-toiminnallisuudella
  public function delete($id) {
    $ostos_model = new OstosModel();
    $ostos_model->delete($id);

    return redirect('ostos');
  }

  // poistetaan tieto omalla remove-funktiolla
  public function remove($id) {
    $ostos_model = new OstosModel();
    $ostos_model->remove($id);

    return redirect('ostos');
  }

  public function update($id) {
    $ostos_model = new OstosModel();

    // vaihtoehtoinen tapa
    //$data = $ostos_model->haeOstos($id);

    // codeigniterin sisäänrakennettu CRUD-Read
    $data = $ostos_model->find($id);

    return view('editOstos_view', $data);
  }

  public function updateRivi() {
    $ostos_model = new OstosModel();

    // tallennetaan formin tiedot omiin muuttujiin
    $id = $this->request->getVar('id');
    $kuvaus = $this->request->getVar('kuvaus');

    //$timestamp = date('Y-m-d H:i:s');

    $data = [
      'kuvaus' => $kuvaus
    ];

    // CodeIgniterin sisäänrakennettu update
    $ostos_model->update($id, $data);

    return redirect('ostos');
  }

}
