<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Document</title>
</head>
<body>
  <h3>Ostoslista (<?= $maara; ?>)</h3>
  <form action="/ostos/lisaa">
    <label>Kuvaus</label>
    <input name="kuvaus">
    <button>Lisää</button>
  </form>
  <ol>
  <?php foreach($ostokset as $ostos): ?>
    <li>
        <?= $ostos['kuvaus'];?> <?= date('d.m.Y H.i',
        strtotime($ostos['tallennettu']));?> 
        <?= anchor("ostos/remove/" . $ostos['id'], "DELETE"); ?>
        <?= anchor("ostos/update/" . $ostos['id'], "UPDATE"); ?>
      
    </li>
  <?php endforeach;?>
  </ol>
</body>
</html>