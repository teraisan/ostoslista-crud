<?php namespace App\Models;

use CodeIgniter\Model;

class OstosModel extends Model {
  protected $table = 'ostos';

  protected $allowedFields = ['kuvaus'];

  public function haeOstokset() {
    return $this->findAll();
  }

  // itse tehty query builderilla toimiva rivin poisto
  public function remove($id) {
  	$this->where('id', $id);
  	$this->delete();
  }

  // tehdään sql-kysey itse ja haetaan tiedot kannasta
  public function haeOstos($id) {
  	$sql = "SELECT * FROM ostos WHERE id = " . $id;
  	$query = $this->query($sql);

  	// palautetaan tiedot taulukkona
  	return $query->getRowArray();
  }

  // CRUD-toiminnallisuudet
  // C = CREATE => INSERT INTO ....
  // R = READ => SELECT * FROM ...
  // U = UPDATE => UPDATE TABLE ....
  // D = DELETE => DELETE FROM 


}