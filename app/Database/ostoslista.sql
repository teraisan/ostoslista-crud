create database if not exists ostoslista;

use ostoslista;

create table ostos (
  id int primary key auto_increment,
  kuvaus varchar(255) not null,
  tallennettu timestamp default current_timestamp
);

insert into ostos (kuvaus) value ('testi');